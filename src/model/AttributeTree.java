package model;

import java.util.Vector;

import weka.core.Instances;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.NumericToNominal;

public class AttributeTree 
{
	private String[] m_attributes;
	private String[][] m_values;
	private int[][] m_valueCount;
	private boolean[][] m_valuesToUse;
	
	public AttributeTree(Instances data)
	{
		this.m_attributes = new String[data.numAttributes()];
		this.m_values = new String[data.numAttributes()][];
		this.m_valueCount = new int[data.numAttributes()][];
		this.m_valuesToUse = new boolean[data.numAttributes()][];
		
		//Find the numeric attributes
		Vector<Integer> numericValues = new Vector<Integer>();
		
		for(int i = 0; i < data.numAttributes(); i++)
		{
			if(data.attribute(i).isNumeric() == true)
			{
				numericValues.add(i);
			}		
		}
		
		//Turn the numeric attributes to nominal attributes
		if(numericValues.size() > 0)
		{
			NumericToNominal filter = new NumericToNominal();
			int[] numericValuesArray = new int[numericValues.size()];
			
			for(int i = 0; i < numericValues.size(); i++)
			{
				numericValuesArray[i] = numericValues.get(i);
			}
			
			filter.setAttributeIndicesArray(numericValuesArray);
			try 
			{
				filter.setInputFormat(data);
				data = Filter.useFilter(data, filter);
			} 
			catch (Exception e) {
				System.out.println("Failed to convert numeric attribute");
				e.printStackTrace();
			}
		}
		
		//Add the attributes to the attribute tree
		for(int i = 0; i < data.numAttributes(); i++)
		{
			if(data.attribute(i).isNominal() == true)
			{
				this.m_attributes[i] = data.attribute(i).name();
				this.m_values[i] = new String[data.attribute(i).numValues()];
				this.m_valueCount[i] = data.attributeStats(i).nominalCounts;
				this.m_valuesToUse[i] = new boolean[data.attribute(i).numValues()];
				
				for(int j = 0; j < data.attribute(i).numValues(); j++)
				{
					this.m_values[i][j] = data.attribute(i).value(j);
					this.m_valuesToUse[i][j] = true;
				}
			}
		}	
	}
	
	public int getNrOfAttributes()
	{
		return this.m_attributes.length;
	}
	
	public String getAttribute(int index) throws IndexOutOfBoundsException
	{
		if(index < 0 || index > this.m_attributes.length - 1)
		{
			throw new IndexOutOfBoundsException();
		}
		
		return this.m_attributes[index];
	}
	
	public String[] getAttributes()
	{
		return this.m_attributes;
	}
	
	public String[][] getAttributeValues()
	{
		return this.m_values;
	}
	
	public int[][] getAttributeValueCount()
	{
		return this.m_valueCount;
	}
	
	public boolean[][] getAttributeValuesToUse()
	{
		return this.m_valuesToUse;
	}
	
	public boolean useAttribute(int index) throws IndexOutOfBoundsException
	{
		if(index < 0 || index > this.m_attributes.length - 1)
		{
			throw new IndexOutOfBoundsException();
		}
		
		for(int i = 0; i < this.m_valuesToUse[index].length; i++)
		{
			// As long as a value should be used, the attribute is then inferred to be used.
			if(this.m_valuesToUse[index][i])
			{
				return true;
			}
		}
		
		return false;
	}
	
	
	public void setValueToUse(int attributeIndex, int valueIndex, boolean useFlag) throws IndexOutOfBoundsException
	{
		if(attributeIndex < 0 || attributeIndex > this.m_attributes.length - 1)
		{
			throw new IndexOutOfBoundsException();
		}
		if(valueIndex < 0 || valueIndex > this.m_valuesToUse[attributeIndex].length - 1)
		{
			throw new IndexOutOfBoundsException();
		}
		
		this.m_valuesToUse[attributeIndex][valueIndex] = useFlag;
	}
	
	public void setAttributeToUse(String attributeName, boolean useFlag) throws Exception
	{
		for(int i = 0; i < this.m_attributes.length; i++)
		{
			if(this.m_attributes[i].equalsIgnoreCase(attributeName))
			{
				for(int j = 0; j < this.m_valuesToUse[i].length; j++)
				{
					this.m_valuesToUse[i][j] = useFlag;
				}
				
				return;
			}
		}
		
		throw new Exception("Attribute: '" + attributeName + "' not found!");
	}
	
	public int getAttributeIndex(String attributeName)
	{
		for(int i = 0; i < this.m_attributes.length; i++)
		{
			if(this.m_attributes[i].equalsIgnoreCase(attributeName))
			{
				return i;
			}
		}
		
		return -1;
	}
	
	
	public String toString()
	{
		// Ex:
		// Attribute 1			Attribute 2
		// Value 1 - useflag	value 1 - useflag
		// value 2 - useflag	

		String text = "";
		int mostValues = 0;
		int[] offsets = new int[this.m_values.length];
		// Set default values.
		for(int i = 0; i < this.m_values.length; i++)
		{
			offsets[i] = 8; // " - false". Check done later to set to 7 if true.
		}
		int[] maxValueLengths = new int[this.m_values.length];
		// Find out necessary info for the next step.
		for(int i = 0; i < this.m_attributes.length; i++)
		{
			mostValues = Math.max(mostValues, this.m_values[i].length);
			for(int j = 0; j < this.m_values[i].length; j++)
			{
				maxValueLengths[i] = Math.max(maxValueLengths[i], this.m_values[i][j].length());
				// Check if any value is used (aka true).
				if(!this.m_valuesToUse[i][j])
				{
					offsets[i] = 7;
				}
			}
		}
		// Write attribute names.
		for(int i = 0; i < this.m_attributes.length; i++)
		{
			String attribute = this.m_attributes[i];
			int diff = maxValueLengths[i] - attribute.length();
			// Add space to the attribute until they become aligned with the coming values.
			for(int j = 0; j < diff + offsets[i]; j++)
			{
				attribute += " ";
			}
			text += attribute + "\t";
		}
		text += "\n";
		// Write the attribute values and if they were used or not.
		// Iterate as many times as the most number of values an attribute has.
		for(int rowOrValueIndex = 0; rowOrValueIndex < mostValues; rowOrValueIndex++)
		{
			// On each row, write the attribute value and if it was used or not.
			for(int attributeIndex = 0; attributeIndex < this.m_attributes.length; attributeIndex++)
			{
				String value = "";
				if(rowOrValueIndex < this.m_values[attributeIndex].length)
				{
					value = this.m_values[attributeIndex][rowOrValueIndex];
					int diff = maxValueLengths[attributeIndex] - value.length();
					diff += offsets[attributeIndex] - 7; // Add 0 or 1 depending on if offset is 7 or 8.
					// Add space to the value until they become aligned.
					for(int k = 0; k < diff; k++)
					{
						value += " ";
					}
					
					text += value + " - " + this.m_valuesToUse[attributeIndex][rowOrValueIndex] + "\t";
				}
				else
				{
					// No value; fill with spaces only.
					for(int k = 0; k < maxValueLengths[attributeIndex] + offsets[attributeIndex]; k++)
					{
						value += " ";
					}
					
					text += value + "\t";
				}
			}
			text += "\n";
		}
		
		return text;
	}
}
