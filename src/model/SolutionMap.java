package model;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import model.MachineLearner.DecisionList;
import model.MachineLearner.DecisionList.Rule;
import model.MachineLearner.DecisionList.Rule.Condition;

public class SolutionMap {

	private class Mapping
	{
		private ArrayList<MachineLearner.DecisionList.Rule.Condition> m_conditions;
		private String m_solution;

		public Mapping(ArrayList<Condition> conditions, String solution)
		{
			m_conditions = conditions;
			m_solution = solution;
		}

		public boolean subsetOf(Rule rule)
		{
			// TODO Collection<>.contains...? doh

			Iterator<Condition> i = m_conditions.iterator();
			while ( i.hasNext() )
			{
				Condition mc = i.next();

				// Linear search for mc in rule
				boolean found = false;
				Iterator<Condition> j = rule.getConditions().iterator();
				while ( j.hasNext() && !found )
				{
					Condition rc = j.next();

					if ( mc.equals(rc) )
					{
						found = true;
					}
				}
				if ( !found )
				{
					return false;
				}
			}
			return true;
		}

		public String getSolution()
		{
			return m_solution;
		}
	}

	private ArrayList<Mapping> m_mappings = new ArrayList<Mapping>();

	public SolutionMap(String filePath) throws IOException
	{
		BufferedReader reader = new BufferedReader(new FileReader(filePath));

		Pattern condPat = Pattern.compile("([A-Za-z0-9_]+)([!<>=]{0,1}=)([A-Za-z0-9_]+)");
		try
		{
			String line;
			while ( (line = reader.readLine()) != null )
			{
				// Strip comments
				int commentIndex = line.indexOf('#');
				if ( commentIndex != -1 )
				{
					line = line.substring(0, commentIndex);
				}
				
				String[] parts = line.split(":", 2);
				if ( parts.length != 2 )
				{
					continue; // silently drops incorrectly formatted mappings (maybe TODO change this)
				}

				// Parse conditions
				Matcher condMatcher = condPat.matcher(parts[0]);

				ArrayList<Condition> conditions = new ArrayList<Condition>();
				while ( condMatcher.find() )
				{
					Condition condition = new Condition(condMatcher.group(1),
													    condMatcher.group(2),
													    condMatcher.group(3));
					conditions.add(condition);
				}
				if ( !conditions.isEmpty() )
				{
					m_mappings.add(new Mapping(conditions, parts[1]));
				}
			}
		} finally
		{
			reader.close();
		}
	}

	public String matches(DecisionList decisionList)
	{
		StringBuilder builder = new StringBuilder();

		Iterator<Rule> i = decisionList.getRules().iterator();
		while ( i.hasNext() )
		{
			Rule rule = i.next();

			rule.appendString(builder);
			builder.append(System.lineSeparator());

			int matchId = 1;
			Iterator<Mapping> j = m_mappings.iterator();
			while ( j.hasNext() )
			{
				Mapping mapping = j.next();

				if ( mapping.subsetOf(rule) )
				{
					builder.append("\t");
					builder.append(matchId++);
					builder.append(". ");
					builder.append(mapping.getSolution());
					builder.append(System.lineSeparator());
				}
			}

			builder.append(System.lineSeparator());
		}

		return builder.toString();
	}
}
