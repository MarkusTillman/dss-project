package model;

public class DatasetFilter
{
	private AttributeTree attributeTree;
	
	public DatasetFilter(AttributeTree attributeTree)
	{
		this.attributeTree = attributeTree;
	}
	
	public AttributeTree getAttributeTree()
	{
		return this.attributeTree;
	}
	
	public String toString()
	{
		String toString = "";
		
		toString += this.attributeTree.toString();
		
		return toString;
	}
}
