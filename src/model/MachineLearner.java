package model;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import weka.classifiers.Classifier;
import weka.classifiers.rules.PART;
import weka.core.Instances;
import weka.filters.Filter;
import weka.filters.unsupervised.instance.RemoveWithValues;

public class MachineLearner
{
	private Instances m_data = null;
	private Instances m_filteredData = null;
	private Classifier m_classifier = new PART();
	private int m_classIndex = -1;
	
	private String getAttributeInformationTAS()
	{
		// Read attribute information from file.
		FileReader fileReader = null; 
		try 
		{
			fileReader = new FileReader("src/data/TAS_MetaData.txt");
		} 
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
			return "Failed to open attribute informations file.";
		}
		BufferedReader bufferedReader = new BufferedReader(fileReader);
		
		// Put information in a hash map.
		Map<String, String> map = new HashMap<String, String>();
		String line = "";
		while(line != null)
		{
			try 
			{
				line = bufferedReader.readLine();
				if(line != null && !line.equals(""))
				{
					String key = line.split(";")[0];
					String value = line.split(";")[1];
					map.put(key, value);
				}
			} 
			catch (IOException e) 
			{
				e.printStackTrace();
			}
		}

		// Output information.
		String text = "";
		text += "0. Missing attribute values are indicated with a question mark '?'.\n";
		int index = 1;
		for(int i = 0; i < this.m_filteredData.numAttributes(); i++)
		{
			// Attribute explanation (if it exists).
			String attributeName = this.m_filteredData.attribute(i).name();
			String attributeDesc = map.get(attributeName);
			if(attributeDesc != null)
			{
				text += index++ + ". " + attributeName + "(" + attributeDesc + "):\n";
				for(int j = 0; j < this.m_filteredData.attribute(i).numValues(); j++)
				{
					String attributeValueName = this.m_filteredData.attribute(i).value(j);
					String attributeValueDesc = map.get(attributeValueName);
					if(attributeValueDesc != null)
					{
						// As long as one instance has the attribute value, it shall be added.
						boolean addInfo = false;
						for(int k = 0; k < this.m_filteredData.numInstances(); k++)
						{
							if(this.m_filteredData.instance(k).stringValue(i).equalsIgnoreCase(attributeValueName))
							{
								addInfo = true;
								break;
							}
						}
						if(addInfo)
						{
							text += "	" + attributeValueName + ": " + attributeValueDesc + "\n";
						}
					}
				}
			}
		}
		
		try 
		{
			bufferedReader.close();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		
		return text;
	}
	public String getAttributeInformation()
	{
		String text = "";
		if(this.m_data.relationName().equalsIgnoreCase("TrafficAccidentsSweden"))
		{
			text = getAttributeInformationTAS();
		}
		else
		{
			text = "Unknown.";
		}
		return text;
	}
	
	public DecisionList generateRules(ParametersPART params) throws Exception
	{	
		if ( m_filteredData == null )
		{
			throw new RuntimeException("Data not filtered yet");
		}
		// Set class index to the last attribute if it has not already been set.
		if(this.m_classIndex < 0)
		{
			this.m_classIndex = this.m_filteredData.numAttributes() - 1;
		}
		m_filteredData.setClassIndex(this.m_classIndex);
		m_classifier.setOptions(params.getOptions());
		m_classifier.buildClassifier(m_filteredData);
		
		String resultRaw = m_classifier.toString();
		return parseDecisionList(resultRaw);
	}
	
	static public class DecisionList
	{
		private ArrayList<Rule> m_rules = new ArrayList<Rule>();

		static public class Rule
		{
			private ArrayList<Condition> m_conditions = new ArrayList<Condition>();
			private String m_label;

			static public class Condition
			{
				private String m_attributeLHS;
				private String m_attributeRHS;
				private String m_op;

				public Condition(String attributeLHS, String op, String attributeRHS)
				{
					m_attributeLHS = attributeLHS;
					m_attributeRHS = attributeRHS;
					m_op = op;
				}

				public String getLHS()
				{
					return m_attributeLHS;
				}

				public String getRHS()
				{
					return m_attributeRHS;
				}

				public String getOp()
				{
					return m_op;
				}

				public boolean equals(Condition cond)
				{
					if ( !this.m_attributeLHS.equals(cond.m_attributeLHS) )
					{
						return false;
					}
					if ( !this.m_attributeRHS.equals(cond.m_attributeRHS) )
					{
						return false;
					}
					if ( !this.m_op.equals(cond.m_op) )
					{
						return false;
					}
					return true;
				}
			}

			public void addCondition(Condition condition)
			{
				m_conditions.add(condition);
			}

			public ArrayList<Condition> getConditions()
			{
				return m_conditions;
			}

			public void setLabel(String label)
			{
				m_label = label;
			}

			public String getLabel()
			{
				return m_label;
			}

			public String toString()
			{
				StringBuilder builder = new StringBuilder();
				appendString(builder);
				return builder.toString();
			}

			public void appendString(StringBuilder builder)
			{
				HashMap<String, String> opMap = new HashMap<String, String>(); // TODO don't create this every time..
				opMap.put("=", " equals ");
				opMap.put("!=", " doesn't equal ");
				opMap.put("<", " is less than ");
				opMap.put(">", " is greater than ");
				opMap.put("<=", " is less than or eual to ");
				opMap.put(">=", " is greater than or equal to ");

				if ( m_conditions.isEmpty() )
				{
					// Default-rule
					builder.append("If none of the above rules apply, the accident is most likely of type ");
					builder.append(m_label);
				} else
				{
					builder.append("If ");
					Iterator<Rule.Condition> j = m_conditions.iterator();
					while ( j.hasNext() )
					{
						Rule.Condition condition = j.next();

						builder.append(condition.getLHS());
						builder.append(opMap.get(condition.getOp()));
						builder.append(condition.getRHS());

						if ( j.hasNext() )
						{
							builder.append(" AND ");
						}
					}
					builder.append(" THEN an accident is most likely of type ");
					builder.append(m_label);
				}
			}
		}

		public void addRule(Rule rule)
		{
			m_rules.add(rule);
		}

		public ArrayList<Rule> getRules()
		{
			return m_rules;
		}

		public String toString()
		{
			return toNaturalLangString();
		}

		public String toNaturalLangString()
		{
			StringBuilder builder = new StringBuilder();

			Iterator<Rule> i = getRules().iterator();
			while ( i.hasNext() )
			{
				Rule rule = i.next();

				rule.appendString(builder);

				builder.append(System.lineSeparator());
				builder.append(System.lineSeparator());
			}

			return builder.toString();
		}
	}

	private DecisionList parseDecisionList(String source) throws IOException
	{
		DecisionList dest = new DecisionList();
		BufferedReader reader = new BufferedReader(new StringReader(source));
		try
		{
			reader.readLine();
			reader.readLine();
			reader.readLine();

			DecisionList.Rule rule;
			do
			{
				rule = parseRule(reader);
				dest.addRule(rule);
			} while ( reader.ready() && !rule.getConditions().isEmpty() );
		} finally
		{
			reader.close();
		}

		return dest;
	}
	
	private DecisionList.Rule parseRule(BufferedReader reader) throws IOException
	{
		DecisionList.Rule rule = new DecisionList.Rule();

		String line;
		while ( (line = reader.readLine()) != null && !line.isEmpty() )
		{
			StringTokenizer tokenizer = new StringTokenizer(line, " :");
			int numTokens = tokenizer.countTokens();
			if ( numTokens == 2 )
			{
				// assume default rule
				String label = tokenizer.nextToken();
				String accuracy = tokenizer.nextToken(); // TODO use?

				rule.setLabel(label);
				break;
			}

			String lhs = tokenizer.nextToken();
			String op = tokenizer.nextToken();
			if ( !op.equals("=") && !op.equals("!=") && !op.equals(">") &&
				 !op.equals("<") && !op.equals(">=") && !op.equals("<=") )
			{
				throw new RuntimeException("Rule parser encountered an unexpected operator");
			}
			String rhs = tokenizer.nextToken();

			rule.addCondition(new DecisionList.Rule.Condition(lhs, op, rhs));

			if ( numTokens == 4 )
			{
				// 4 tokens -> assume (but verify) AND
				String and = tokenizer.nextToken();
				if ( !and.equals("AND") )
				{
					throw new RuntimeException("Rule parser encountered an unexpected token: "+and);
				}
			} else if ( numTokens == 5 )
			{
				// 5 tokens -> assume label and accuracy
				String label = tokenizer.nextToken();
				rule.setLabel(label);

				String accuracy = tokenizer.nextToken(); // TODO use?
				rule.setLabel(label);
			}
		}

		return rule;
	}

	public AttributeTree loadData(String filePath) throws IOException
	{
		AttributeTree result;
		
		this.m_data = null;
		this.m_filteredData = null;
		
		// Check if the extension is ARFF.
		String[] splitsFilePath = filePath.split("\\.");
		String fileExtension = splitsFilePath[splitsFilePath.length - 1];
		
		if(fileExtension.compareTo("arff") != 0)
		{
			throw new IOException("Input file is not an arff file");
		}

		FileReader fr = new FileReader(filePath);
		m_data = new Instances(fr);
		fr.close();	
		
		//Remove all attributes that are not nominal
		for(int i = 0; i < this.m_data.numAttributes(); i++)
		{
			if(this.m_data.attribute(i).isNominal() == false && this.m_data.attribute(i).isNumeric() == false)
			{
				this.m_data.deleteAttributeAt(i);
				i--;
			}
		}
		
		//Set the filtered data to data
		this.m_filteredData = new Instances(this.m_data);
			
		//Create the attribute tree
		result = new AttributeTree(this.m_data);
		
		return result;
	}
	
	//Filters the data
	public void filterAndScan(DatasetFilter filter)
	{
		this.m_filteredData = new Instances(this.m_data);
		
		for(int i = filter.getAttributeTree().getNrOfAttributes() - 1; i >= 0; i--)
		{		
			if(filter.getAttributeTree().useAttribute(i) == true)
			{
				if(this.m_filteredData.attribute(i).isNominal() == true)
				{
					//Find the values that are going to be filtered
					String values = "";
					
					for(int j = 0; j < filter.getAttributeTree().getAttributeValuesToUse()[i].length; j++)
					{
						if(filter.getAttributeTree().getAttributeValuesToUse()[i][j] == false)
						{
							values += (j+1) + ",";
						}
					}
					
					if(values.length() > 0)
					{
						//Remove the last comma
						values = values.substring(0, values.length() - 1);
						
						//Create the filter
						RemoveWithValues instanceFilter = new RemoveWithValues();
						
						weka.core.Attribute attributeIndex = this.m_filteredData.attribute(filter.getAttributeTree().getAttribute(i));
						
						if(attributeIndex != null)
						{
							instanceFilter.setAttributeIndex(Integer.toString(attributeIndex.index() + 1));
						}					
							
						instanceFilter.setNominalIndices(values);							
						instanceFilter.setModifyHeader(true);
						try 
						{
							//Run the filter						
							instanceFilter.setInputFormat(this.m_filteredData);
							this.m_filteredData = Filter.useFilter(this.m_filteredData, instanceFilter);
						} 
						catch (Exception e) 
						{
							System.out.println("Filter failed: " + e.toString());
							e.printStackTrace();
						}
					}
				}
				//If the value is numeric
				else
				{
					for(int j = 0; j < filter.getAttributeTree().getAttributeValuesToUse()[i].length; j++)
					{
						if(filter.getAttributeTree().getAttributeValuesToUse()[i][j] == false)
						{
							for(int k = this.m_filteredData.numInstances() - 1; k >= 0; k--)
							{
								//Check if the instance has the given value and, if it does, remove it
								if(this.m_filteredData.instance(k).value(i) == Integer.parseInt(filter.getAttributeTree().getAttributeValues()[i][j]))
								{
									this.m_filteredData.delete(k);
								}
							}	
						}
					}					
				}
			}
			else	
			{
				//Remove the attribute if it exists
				weka.core.Attribute a = this.m_filteredData.attribute(filter.getAttributeTree().getAttribute(i));
				
				if(a != null)
				{
					this.m_filteredData.deleteAttributeAt(a.index());
				}
			}
		}
	}
	
	//Returns information about the data
	public String getInstancesAsString()
	{
		String result = "INSTANCES\n";
		
		if(this.m_filteredData != null)
		{
			//Add instance data
			for(int i = 0; i < this.m_filteredData.numAttributes(); i++)
			{
				result += this.m_filteredData.attribute(i).name().toUpperCase().replace('_', ' ') + "\t";
			}
			
			result += "\n";
			
			for(int i = 0; i < this.m_filteredData.numInstances(); i++)
			{
				for(int j = 0; j < this.m_filteredData.numAttributes(); j++)
				{
					result += this.m_filteredData.instance(i).stringValue(j).replace('_', ' ') + "\t";
				}
				
				result += "\n";
			}
		}
		
		return result;
	}
		
	public String getInstanceInformation()
	{
		String result = "";
		
		if(this.m_filteredData != null)
		{
			// Explanation of instance data.
			result += "ATTRIBUTE INFORMATION\n";
			result += getAttributeInformation();
			result += "\n";
			
			//Missing values
			/*result += "\n------------------------------------------------------------------------------------------------------\n";
			result += "MISSING VALUES\n";
			
			for(int i = 0; i < this.m_filteredData.numAttributes(); i++)
			{
				result += this.m_filteredData.attribute(i).name().replace('_', ' ') + ": " + (float)this.m_filteredData.attributeStats(i).missingCount / (float)this.m_filteredData.numInstances() + "%\n";
			}*/
			
			//Statistics
			result += "\n------------------------------------------------------------------------------------------------------\n";
			result += "STATISTICS\n";
			
			result += "Number of instances: " + this.m_filteredData.numInstances() + "\n";
			result += "Number of attributes: " + this.m_filteredData.numAttributes() + "\n\n";
			
			/*for(int i = 0; i < this.m_filteredData.numAttributes(); i++)
			{
				int[] nominalCounts = this.m_filteredData.attributeStats(i).nominalCounts;
				
				result += this.m_filteredData.attribute(i).name().replace('_', ' ') + ": ";
				
				for(int j = 0; j < nominalCounts.length; j++)
				{
					result += this.m_filteredData.attribute(i).value(j).replace('_', ' ') + "(" + nominalCounts[j] + "), ";		
				}
				
				result += "Missing(" + this.m_filteredData.attributeStats(i).missingCount + ")\n";
			}	*/		
		}

		return result;		
	}
	
	public boolean hasDataset()
	{
		return this.m_data != null;
	}
	
	public String[] getAttributes()
	{
		String[] result = new String[this.m_filteredData.numAttributes()];
		
		for(int i = 0; i < this.m_filteredData.numAttributes(); i++)
		{
			result[i] = this.m_filteredData.attribute(i).name().replace('_', ' ');
		}	
		
		return result;
	}
	
	public AttributeTree getAttributeTree()
	{
		return new AttributeTree(new Instances(this.m_filteredData));
	}
	
	public String[][] getAttibuteValues()
	{			
		return new AttributeTree(new Instances(this.m_filteredData)).getAttributeValues();
	}
	
	public String[][] getInstances()
	{
		String[][] result = new String[this.m_filteredData.numInstances()][this.m_filteredData.numAttributes()];
		
		for(int i = 0; i < result.length; i++)
		{
			for(int j = 0; j < result[i].length; j++)
			{
				if(this.m_filteredData.attribute(j).isNominal() == true)
				{
					result[i][j] = this.m_filteredData.instance(i).stringValue(j).replace('_', ' ');
				}
				else
				{
					//The value is cast to an int since all numeric attributes in the dataset are integer values anyway
					if(this.m_filteredData.instance(i).isMissing(j) == true)
					{
						result[i][j] = "?";
					}
					else
					{
						result[i][j] = Integer.toString((int)this.m_filteredData.instance(i).value(j));
					}
				}
			}
		}	
		
		return result;
	}
	
	public int[][] getAttributeValueCount()
	{
		return new AttributeTree(new Instances(this.m_filteredData)).getAttributeValueCount();
	}
	
	public int[] getMissingValues()
	{
		int[] result = new int[this.m_filteredData.numAttributes()];
		
		for(int i = 0; i < this.m_filteredData.numAttributes(); i++)
		{
			result[i] = this.m_filteredData.attributeStats(i).missingCount;
		}		
		
		return result;		
	}
	
	public int getNrOfInstances()
	{
		return this.m_filteredData.numInstances();
	}
	
	public int getNrOfAttributes()
	{
		return this.m_filteredData.numAttributes();
	}
	
	public void setClassIndex(int classIndex) throws IndexOutOfBoundsException
	{
		if(classIndex < 0 || classIndex > this.m_filteredData.numAttributes() - 1)
		{
			throw new IndexOutOfBoundsException();
		}
		
		this.m_classIndex = classIndex;
	}
}
