package model;

import java.util.ArrayList;

public class ParametersPART
{
	private boolean binarySplits;
	private float confidenceFactor;
	private int minNumObj;
	private int numFolds;
	private boolean reducedErrorPruning;
	private int seed;
	private boolean unpruned;
	
	private String[] optionsArray = null; // cached array

	public ParametersPART()
	{
		this.binarySplits = false;
		this.confidenceFactor = 0.25f;
		this.minNumObj = 2;
		this.numFolds = 3;
		this.reducedErrorPruning = false;
		this.seed = 1;
		this.unpruned = false;
	}
	public ParametersPART(boolean binarySplits, float confidenceFactor, int minNumObj, int numFolds, boolean reducedErrorPruning, int seed, boolean unpruned)
	{
		this.binarySplits = binarySplits;
		this.confidenceFactor = confidenceFactor;
		this.minNumObj = minNumObj;
		this.numFolds = numFolds;
		this.reducedErrorPruning = reducedErrorPruning;
		this.seed = seed;
		this.unpruned = unpruned;
	}
	
	public boolean getBinarySplitsFlag()
	{
		return this.binarySplits;
	}
	public float getConfidenceFactor()
	{
		return this.confidenceFactor;
	}
	public int getMinNumObj()
	{
		return this.minNumObj;
	}
	public int getNumFolds()
	{
		return this.numFolds;
	}
	public boolean getReducedErrorPruningFlag()
	{
		return this.reducedErrorPruning;
	}
	public int getSeed()
	{
		return this.seed;
	}
	public boolean getUnprunedFlag()
	{
		return this.unpruned;
	}
	// Set.
	public void setBinarySplitsFlag(boolean binarySplitsFlag)
	{
		this.binarySplits = binarySplitsFlag;
		this.optionsArray = null;
	}
	public void setConfidenceFactor(float confidenceFactor)
	{
		this.confidenceFactor = confidenceFactor;
		this.optionsArray = null;
	}
	public void setMinNumObj(int minNumObj)
	{
		this.minNumObj = minNumObj;
		this.optionsArray = null;
	}
	public void setNumFolds(int numFolds)
	{
		this.numFolds = numFolds;
		this.optionsArray = null;
	}
	public void setReducedErrorPruningFlag(boolean reducedErrorPruningFlag)
	{
		this.reducedErrorPruning = reducedErrorPruningFlag;
		this.optionsArray = null;
	}
	public void setSeed(int seed)
	{
		this.seed = seed;
		this.optionsArray = null;
	}
	public void setUnprunedFlag(boolean unprunedFlag)
	{
		this.unpruned = unprunedFlag;
		this.optionsArray = null;
	}
	public String[] getOptions()
	{
		if ( optionsArray == null )
		{
			ArrayList<String> list = new ArrayList<String>();

			list.add("-M");
			list.add(""+minNumObj);
			if ( reducedErrorPruning )
			{
				list.add("-R");

				list.add("-N");
				list.add(""+numFolds);
			} else
			{
				list.add("-C");
				list.add(""+confidenceFactor);
			}
			if ( binarySplits )
			{
				list.add("-B");
			}
			if ( unpruned )
			{
				list.add("-U");
			}
			list.add("-Q");
			list.add(""+seed);

			optionsArray = list.toArray(new String[list.size()]);
		}

		return optionsArray;
	}
}

