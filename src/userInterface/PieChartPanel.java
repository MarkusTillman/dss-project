package userInterface;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.util.Random;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class PieChartPanel extends JPanel 
{
	public PieChartPanel(String attributeName, String attributeValueNames[], int attributeValueCounts[], int missingValues)
	{		
		this.setLayout(new GridBagLayout());
		this.setOpaque(true);		
		this.setBackground(Color.WHITE);
		//this.setMinimumSize(new Dimension(500, 300));
		
		//Create the pie chart
		GridBagConstraints gbc = new GridBagConstraints();	
		gbc.gridy = 0;
		gbc.gridx = 1;
		gbc.anchor = GridBagConstraints.NORTHEAST;
		gbc.gridheight = 2;
		gbc.fill = GridBagConstraints.BOTH;
		
		Color[] colors = new Color[attributeValueCounts.length];
		float[] percentages = new float[attributeValueCounts.length];
		Random r = new Random(25);
		
		//Generate colors
		for(int j = 0; j < attributeValueNames.length; j++)
		{
			colors[j] = new Color(r.nextInt(255), r.nextInt(255), r.nextInt(255));
			percentages[j] = 100.0f * (float)attributeValueCounts[j] / (float)attributeValueCounts.length;
		}
		
		this.add(new PieChart(300, 300, percentages, colors), gbc);		

		//Add the name of the attribute
		gbc = new GridBagConstraints();
		gbc.anchor = GridBagConstraints.CENTER;
		gbc.gridy = 0;
		gbc.gridx = 0;		
		
		JLabel attributeTitle = new JLabel(attributeName);
		attributeTitle.setFont(attributeTitle.getFont().deriveFont(Font.BOLD));
		
		this.add(attributeTitle, gbc);
		
		//Add some attribute statistics	
		gbc = new GridBagConstraints();
		gbc.anchor = GridBagConstraints.NORTHWEST;
		gbc.gridy = 1;
		gbc.gridx = 0;		
		
		JPanel p = new JPanel();
		p.setLayout(new GridLayout(20, 2, 1, 1));
		p.setOpaque(true);
		p.setBackground(Color.WHITE);
		
		this.add(p, gbc);
		
		//Create labels for the attribute values
		for(int j = 0; j < attributeValueCounts.length; j++)
		{
			JLabel label = new JLabel(attributeValueNames[j] + "(" + attributeValueCounts[j] + ")");
			label.setForeground(colors[j]);

			p.add(label);
		}	
		
		JLabel label = new JLabel("Missing(" + missingValues + ")");
		label.setForeground(Color.LIGHT_GRAY);

		p.add(label);		
	}
}
