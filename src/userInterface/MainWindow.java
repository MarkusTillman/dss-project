package userInterface;

import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.SpringLayout;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.BorderLayout;

import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import java.awt.Color;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.GridLayout;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Random;
import java.util.Vector;

import javax.swing.JTree;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

import model.*;

import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;







// Table test
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import java.awt.Font;

import javax.swing.JTabbedPane;

import java.awt.Insets;

@SuppressWarnings("serial")
public class MainWindow extends JFrame implements ActionListener
{
	// Main window.
	private JFrame selfRef;
	private JPanel panel;
	private GridBagLayout gridBagLayout;
	private JTextArea textAreaOtherTab;
	private JTabbedPane tabbedPane;
	private JTree tree;
	
	// Table test
	private DefaultTableModel instancesTableModel;
	private JTable instancesTable;
	private JPanel otherPanel;	
	private JPanel pieChartPanel;

	// Parameters & parameter window.
	ParametersPART parameters;
	private JDialog parameterDialog;
	private JDialog parameterErrorDialog;
	private int parameterErrorDialogWidth;
	private int parameterErrorDialogHeight;
	private JTextArea parameterErrorTextArea;
	private JCheckBox chckbxBinarysplits;
	private JTextField textFieldConfidenceFactor;
	private JTextField textFieldMinNumObj;
	private JTextField textFieldNumFolds;
	private JCheckBox chckbxReducederrorpruning;
	private JTextField textFieldSeed;
	private JCheckBox chckbxUnpruned;
	
	// Results window
	private JDialog resultsDialog;
	private JTextArea resultsTextArea;
	
	// Set class attribute.
	private JDialog setClassAttributeDialog;
	private ButtonGroup radioButtonGroup;
	
	// Other.
	private JFileChooser fileChooser;
	private MachineLearner learner;
	private AttributeTree attributeTree;
	DatasetFilter datasetFilter;
	private String dataSetFileName;
	private int vSBSpeed; // Vertical scroll speed.
	
	private void createParameterErrorDialog()
	{
		JPanel panel = new JPanel();
		this.parameterErrorDialog = new JDialog(this, "Error!");
		this.parameterErrorTextArea = new JTextArea();
		panel.setLayout(null);
	
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.getVerticalScrollBar().setUnitIncrement(vSBSpeed);
		scrollPane.setBounds(0, 0, this.parameterErrorDialogWidth - 6, this.parameterErrorDialogHeight - 28);
		panel.add(scrollPane);
		scrollPane.setViewportView(this.parameterErrorTextArea);
		
		this.parameterErrorDialog.setSize(this.parameterErrorDialogWidth, this.parameterErrorDialogHeight);
		this.parameterErrorDialog.setLocation(this.getLocation().x + this.getWidth() / 2 - this.parameterErrorDialogWidth / 2, this.getLocation().y + this.getHeight() / 2 - this.parameterErrorDialogHeight / 2);
		this.parameterErrorDialog.setVisible(true);
		this.parameterErrorDialog.setResizable(false);
		this.parameterErrorDialog.setContentPane(panel);
	}
	
	private void loadAttributeTreeFromGUITree(AttributeTree attributeTree)
	{
		String[] attributeNames = attributeTree.getAttributes();
		String[][] attributeValues = attributeTree.getAttributeValues();
		
		// Expand tree to be able to read paths.
		// First find out the maximum number of nodes. ev. todo: more efficient solution.
		int maxNrOfNodes = 0;
		for(int i = 0; i < attributeNames.length; i++)
		{
			for(int j = 0; j < attributeValues[i].length; j++)
			{
				maxNrOfNodes++;
			}
		}
		maxNrOfNodes += tree.getRowCount();
		
		// Check which nodes shall be collapsed when we're done. (Rowcount increases as rows are expanded).
		boolean[] toCollapse = new boolean[maxNrOfNodes];
		for(int i = 0; i < tree.getRowCount(); i++)
	    {
	    	if(!tree.isExpanded(i))
	    	{
	    		toCollapse[i] = true;
	    	}
	    	else
	    	{
	    		toCollapse[i] = false;
	    	}
	    }
		// Expand the nodes. (Rowcount increases as rows are expanded).
		for(int i = 0; i < tree.getRowCount(); i++)
	    {
	    	if(!tree.isExpanded(i))
	    	{
	    		tree.expandRow(i);
	    	}
	    }
		
		// Read paths and extract values.
		int pathIndex = 0;
		for(int i = 0; i < attributeNames.length; i++)
		{
			pathIndex++; // Skip attribute NAME.
			for(int j = 0; j < attributeValues[i].length; j++)
			{
				boolean checkValue = tree.getPathForRow(pathIndex++).getLastPathComponent().toString().contains("true");
				attributeTree.setValueToUse(i, j, checkValue);
			}
		}
		
		// Restore tree to previous state.
		for(int i = 0; i < maxNrOfNodes; i++) 
	    {
			if(toCollapse[i])
			{
				tree.collapseRow(i);
			}
	    }
	}
	
	
	public MainWindow(boolean useSystemLookAndFeel) 
	{
		if(useSystemLookAndFeel)
		{
			try 
			{
				UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			} 
			catch (ClassNotFoundException e1) 
			{
				e1.printStackTrace();
			} 
			catch (InstantiationException e1) 
			{
				e1.printStackTrace();
			} 
			catch (IllegalAccessException e1) 
			{
				e1.printStackTrace();
			} 
			catch (UnsupportedLookAndFeelException e1) 
			{
				e1.printStackTrace();
			}
		}
		
		// Main window.
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		this.setTitle("Traffic accident decision support system. TODO: NAME");
		panel = new JPanel();
		panel.setBackground(Color.LIGHT_GRAY);
		selfRef = this;
		gridBagLayout = new GridBagLayout();
		tree = null;
		
		// Table test
		this.instancesTableModel = new DefaultTableModel();
		this.instancesTable = new JTable(this.instancesTableModel);
		this.instancesTable.setAutoCreateRowSorter(true);
		this.otherPanel = new JPanel();
		this.pieChartPanel = new JPanel();
		
		// Parameters & parameter window.
		parameters = null;
		parameterDialog = null;
		parameterErrorDialog = null;
		parameterErrorDialogWidth = 450;
		parameterErrorDialogHeight = 300;
		parameterErrorTextArea = null;
		chckbxBinarysplits = null;
		textFieldConfidenceFactor = null;
		textFieldMinNumObj = null;
		textFieldNumFolds = null;
		chckbxReducederrorpruning = null;
		textFieldSeed = null;
		chckbxUnpruned = null;
		
		// Results window.
		resultsDialog = null;
		resultsTextArea = null;

		// Set class attribute.
		setClassAttributeDialog = null;
		radioButtonGroup = new ButtonGroup();
		
		// Other.
		learner = new MachineLearner();
		attributeTree = null;
		fileChooser = new JFileChooser();
		datasetFilter = null;
		vSBSpeed = 16;
		String dataSetFileName = "";
		
		this.setupFrame();
		this.setContentPane(this.panel);
		
		this.panel.updateUI();	
	}
	

	private void setupFrame()
	{

		addComponentListener(new ComponentAdapter() 
		{
			@Override
			public void componentMoved(ComponentEvent arg0) 
			{
				if(parameterDialog != null)
				{
					parameterDialog.setLocation(selfRef.getLocation().x + selfRef.getWidth(), selfRef.getLocation().y);
				}
				if(resultsDialog != null)
				{
					resultsDialog.setLocation(selfRef.getLocation().x + selfRef.getWidth(), selfRef.getLocation().y);
				}
				if(parameterErrorDialog != null)
				{
					parameterErrorDialog.setLocation(selfRef.getLocation().x + selfRef.getWidth() / 2 - parameterErrorDialogWidth / 2, selfRef.getLocation().y + selfRef.getHeight() / 2 - parameterErrorDialogHeight / 2);
				}
			}
			@Override
			public void componentResized(ComponentEvent arg0) 
			{
				if(parameterDialog != null)
				{
					parameterDialog.setLocation(selfRef.getLocation().x + selfRef.getWidth(), selfRef.getLocation().y);
				}
				if(resultsDialog != null)
				{
					resultsDialog.setLocation(selfRef.getLocation().x + selfRef.getWidth(), selfRef.getLocation().y); 
				}
				if(parameterErrorDialog != null)
				{
					parameterErrorDialog.setLocation(selfRef.getLocation().x + selfRef.getWidth() / 2 - parameterErrorDialogWidth / 2, selfRef.getLocation().y + selfRef.getHeight() / 2 - parameterErrorDialogHeight / 2);
				}
			}
		});
		
		
		this.setSize(800, 600);
		this.setVisible(true);
		
		// Menu.
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		JMenu mnMenu = new JMenu("Menu");
		menuBar.add(mnMenu);
		JMenuItem mntmLoadDataset = new JMenuItem("Load dataset...");
		mntmLoadDataset.addActionListener(this);
		mnMenu.add(mntmLoadDataset);
		JMenuItem mntmRun = new JMenuItem("Run...");
		mntmRun.addActionListener(this);
		mnMenu.add(mntmRun);
		JMenuItem mntmExport = new JMenuItem("Export...");
		mntmExport.addActionListener(this);
		mnMenu.add(mntmExport);
		JMenuItem mntmQuit = new JMenuItem("Quit");
		mntmQuit.addActionListener(this);
		mnMenu.add(mntmQuit);
		
		JMenu mnFilter = new JMenu("Filter");
		menuBar.add(mnFilter);
		
		JMenuItem mntmApply = new JMenuItem("Apply");
		mntmApply.addActionListener(this);
		mnFilter.add(mntmApply);
		
		JMenuItem mntmApplyDefault = new JMenuItem("Apply default");
		mntmApplyDefault.addActionListener(this);
		mnFilter.add(mntmApplyDefault);
		
		JMenuItem mntmSetClassAttribute = new JMenuItem("Set class attribute...");
		mntmSetClassAttribute.addActionListener(this);
		mnFilter.add(mntmSetClassAttribute);
		
		// Layout.
		gridBagLayout.columnWidths = new int[]{156, 290, 0};
		gridBagLayout.rowHeights = new int[]{320, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{1.0, Double.MIN_VALUE};
		this.panel.setLayout(gridBagLayout);
		
		// Tabbed pane containing scroll panes containing text areas.
		// Create tabbed pane.
		this.tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		GridBagConstraints gbc_tabbedPane = new GridBagConstraints();
		gbc_tabbedPane.insets = new Insets(0, 0, 0, 5);
		gbc_tabbedPane.fill = GridBagConstraints.BOTH;
		gbc_tabbedPane.gridx = 1;
		gbc_tabbedPane.gridy = 0;
		panel.add(tabbedPane, gbc_tabbedPane);
		// Create text areas.
		textAreaOtherTab = new JTextArea();
		textAreaOtherTab.setEditable(false);
		// Create scroll panes with text areas and add to tabbed pane.
		//JScrollPane scrollPane_1 = new JScrollPane(textAreaInstancesTab);
		JScrollPane scrollPane_1 = new JScrollPane(this.instancesTable);
		scrollPane_1.getVerticalScrollBar().setUnitIncrement(vSBSpeed);
		tabbedPane.addTab("Instances", null, scrollPane_1, null);
		//JScrollPane scrollPane_2 = new JScrollPane(textAreaOtherTab);
		JScrollPane scrollPane_2 = new JScrollPane(this.otherPanel);
		scrollPane_2.getVerticalScrollBar().setUnitIncrement(vSBSpeed);
		tabbedPane.addTab("Other", null, scrollPane_2, null);
		
		this.instancesTable.setEnabled(false);
		//Setup the other panel
		GridBagConstraints gbcOtherPanel = new GridBagConstraints();
		this.otherPanel.setLayout(new GridBagLayout());
		this.otherPanel.setBackground(Color.WHITE);
		
		this.pieChartPanel.setLayout(new GridBagLayout());
		this.pieChartPanel.setOpaque(true);
		this.pieChartPanel.setBackground(Color.WHITE);

		gbcOtherPanel.gridx = 0;
		gbcOtherPanel.gridy = 0;
		gbcOtherPanel.anchor = GridBagConstraints.NORTHWEST;
		gbcOtherPanel.weightx = 1.0;
		this.otherPanel.add(textAreaOtherTab, gbcOtherPanel);
		
		gbcOtherPanel.gridx = 0;
		gbcOtherPanel.gridy = 1;
		gbcOtherPanel.anchor = GridBagConstraints.NORTHWEST;
		gbcOtherPanel.weightx = 1.0;
		this.otherPanel.add(this.pieChartPanel, gbcOtherPanel);
	}

	public void actionPerformed(ActionEvent e) 
	{
		String textOtherTab = "";
		if(e.getActionCommand().equalsIgnoreCase("Load dataset...")) 
		{
			// Set filters.
			FileFilter fileFilter = new FileNameExtensionFilter("Text (.txt)", "txt");
			fileChooser.setFileFilter(fileFilter);
			fileFilter = new FileNameExtensionFilter("Attribute-relation file format (.arff)", "arff");
			fileChooser.setFileFilter(fileFilter);
			
			int returnVal = fileChooser.showOpenDialog(this);
			String filePath = "";
			if(returnVal == JFileChooser.APPROVE_OPTION)
			{
				File file = fileChooser.getSelectedFile();
				fileChooser.setCurrentDirectory(file); // ev. Todo: remember path (not working atm).
				filePath = file.getAbsolutePath();
				this.dataSetFileName = file.getName();
			}
			else
			{
				return;
			}
			
			// Send filepath to machine learner and receive attributes & attribute values.
			try 
			{
				this.attributeTree = this.learner.loadData(filePath);
			} 
			catch (IOException e1) 
			{
				e1.printStackTrace();
				return;
			}
			
			// Create GUI filter (checkbox tree).
			// First create nodes for attribute filter.
			String[] attributeNames = attributeTree.getAttributes();
			String[][] attributeValues = attributeTree.getAttributeValues();
			CheckBoxNode attributeValueNodes[][] = new CheckBoxNode[attributeNames.length][];
			for(int i = 0; i < attributeNames.length; i++)
			{
				attributeValueNodes[i] = new CheckBoxNode[attributeValues[i].length];
				for(int j = 0; j < attributeValues[i].length; j++)
				{
					attributeValueNodes[i][j] = new CheckBoxNode(attributeValues[i][j], true);
					attributeValueNodes[i][j] = new CheckBoxNode(attributeValues[i][j], true);
				}
			}
			
			Object[] attributeNameNodes = new Object[attributeNames.length];
			for(int i = 0; i < attributeNames.length; i++)
			{
				attributeNameNodes[i] = new NamedVector(attributeNames[i], attributeValueNodes[i]);
			}
			Vector root = new NamedVector("Filter", attributeNameNodes);
			
			// Create tree holding the attribute nodes.
		    tree = new JTree(root);
		    CheckBoxNodeRenderer renderer = new CheckBoxNodeRenderer();
		    tree.setCellRenderer(renderer);
		    tree.setCellEditor(new CheckBoxNodeEditor(tree));
		    tree.setEditable(true);
			
		    // Add filter tree to a scrollpane.
			JScrollPane scrollPane_0 = new JScrollPane(tree);
			scrollPane_0.getVerticalScrollBar().setUnitIncrement(vSBSpeed);
			scrollPane_0.setToolTipText("Filter");
			GridBagConstraints gbc_scrollPane = new GridBagConstraints();
			gbc_scrollPane.fill = GridBagConstraints.BOTH;
			gbc_scrollPane.gridx = 0;
			gbc_scrollPane.gridy = 0;
		    this.panel.add(scrollPane_0, gbc_scrollPane);
		    // Don't forget to update the UI to show the new components (tree and nodes).
		    this.panel.updateUI(); 
			
		    // Set attribute tree values from GUI.
			this.loadAttributeTreeFromGUITree(attributeTree);
			
			// Create and send filter to machine learner.
			this.datasetFilter = new DatasetFilter(attributeTree);
			this.learner.filterAndScan(datasetFilter);
			
			// Output filtered data.
			textOtherTab += this.learner.getInstanceInformation();	
			
			//Update the table
			this.instancesTableModel.setDataVector(this.learner.getInstances(), this.learner.getAttributes());
			
			//Create piecharts
			this.updatePieCharts();		
			
			// Set tab containing the table as selected.
			this.tabbedPane.setSelectedIndex(0);
		}
		else if(e.getActionCommand().contains("Run"))
		{
			if(e.getActionCommand().equalsIgnoreCase("run..."))
			{
				// Hide results window, if created.
				if(this.resultsDialog != null)
				{
					this.resultsDialog.setVisible(false);
				}
				
				// First check if a file has been loaded.
				if(!this.learner.hasDataset())
				{
					this.textAreaOtherTab.setText("Can't run; no dataset has been (successfully) loaded!\n");
					this.tabbedPane.setSelectedIndex(1);
					return;
				}
				
				// Create new/show window for setting machine learning parameters.
				if(parameterDialog == null)
				{
					JPanel paramPanel = new JPanel();
					
					parameterDialog = new JDialog(this, "Machine learning parameters.");
					SpringLayout springLayout = new SpringLayout();
					paramPanel.setLayout(springLayout);
					
					
					chckbxBinarysplits = new JCheckBox("binarySplits");
					springLayout.putConstraint(SpringLayout.NORTH, chckbxBinarysplits, 10, SpringLayout.NORTH, this);
					springLayout.putConstraint(SpringLayout.WEST, chckbxBinarysplits, 10, SpringLayout.WEST, this);
					springLayout.putConstraint(SpringLayout.EAST, chckbxBinarysplits, 107, SpringLayout.WEST, this);
					paramPanel.add(chckbxBinarysplits);
					
					textFieldConfidenceFactor = new JTextField();
					springLayout.putConstraint(SpringLayout.NORTH, textFieldConfidenceFactor, 6, SpringLayout.SOUTH, chckbxBinarysplits);
					springLayout.putConstraint(SpringLayout.WEST, textFieldConfidenceFactor, 0, SpringLayout.WEST, chckbxBinarysplits);
					textFieldConfidenceFactor.setText("0.25");
					paramPanel.add(textFieldConfidenceFactor);
					textFieldConfidenceFactor.setColumns(10);
					
					JLabel lblConfidencefactor = new JLabel("confidenceFactor");
					springLayout.putConstraint(SpringLayout.NORTH, lblConfidencefactor, 3, SpringLayout.NORTH, textFieldConfidenceFactor);
					springLayout.putConstraint(SpringLayout.WEST, lblConfidencefactor, 6, SpringLayout.EAST, textFieldConfidenceFactor);
					springLayout.putConstraint(SpringLayout.EAST, lblConfidencefactor, -187, SpringLayout.EAST, this);
					paramPanel.add(lblConfidencefactor);
					
					textFieldMinNumObj = new JTextField();
					springLayout.putConstraint(SpringLayout.NORTH, textFieldMinNumObj, 6, SpringLayout.SOUTH, textFieldConfidenceFactor);
					springLayout.putConstraint(SpringLayout.WEST, textFieldMinNumObj, 0, SpringLayout.WEST, chckbxBinarysplits);
					textFieldMinNumObj.setText("2");
					paramPanel.add(textFieldMinNumObj);
					textFieldMinNumObj.setColumns(10);
					
					JLabel lblMinnumobj = new JLabel("minNumObj");
					springLayout.putConstraint(SpringLayout.NORTH, lblMinnumobj, 3, SpringLayout.NORTH, textFieldMinNumObj);
					springLayout.putConstraint(SpringLayout.WEST, lblMinnumobj, 6, SpringLayout.EAST, textFieldMinNumObj);
					springLayout.putConstraint(SpringLayout.EAST, lblMinnumobj, -209, SpringLayout.EAST, this);
					paramPanel.add(lblMinnumobj);
					
					textFieldNumFolds = new JTextField();
					springLayout.putConstraint(SpringLayout.NORTH, textFieldNumFolds, 6, SpringLayout.SOUTH, textFieldMinNumObj);
					springLayout.putConstraint(SpringLayout.WEST, textFieldNumFolds, 0, SpringLayout.WEST, chckbxBinarysplits);
					textFieldNumFolds.setText("3");
					paramPanel.add(textFieldNumFolds);
					textFieldNumFolds.setColumns(10);
					
					JLabel lblNumfolds = new JLabel("numFolds");
					springLayout.putConstraint(SpringLayout.NORTH, lblNumfolds, 3, SpringLayout.NORTH, textFieldNumFolds);
					springLayout.putConstraint(SpringLayout.WEST, lblNumfolds, 0, SpringLayout.WEST, lblConfidencefactor);
					springLayout.putConstraint(SpringLayout.EAST, lblNumfolds, -209, SpringLayout.EAST, this);
					paramPanel.add(lblNumfolds);
					
					chckbxReducederrorpruning = new JCheckBox("reducedErrorPruning");
					springLayout.putConstraint(SpringLayout.NORTH, chckbxReducederrorpruning, 6, SpringLayout.SOUTH, textFieldNumFolds);
					springLayout.putConstraint(SpringLayout.WEST, chckbxReducederrorpruning, 10, SpringLayout.WEST, this);
					springLayout.putConstraint(SpringLayout.EAST, chckbxReducederrorpruning, -251, SpringLayout.EAST, this);
					paramPanel.add(chckbxReducederrorpruning);
					
					textFieldSeed = new JTextField();
					springLayout.putConstraint(SpringLayout.NORTH, textFieldSeed, 6, SpringLayout.SOUTH, chckbxReducederrorpruning);
					springLayout.putConstraint(SpringLayout.WEST, textFieldSeed, 0, SpringLayout.WEST, chckbxBinarysplits);
					textFieldSeed.setText("1");
					paramPanel.add(textFieldSeed);
					textFieldSeed.setColumns(10);
					
					JLabel lblSeed = new JLabel("seed");
					springLayout.putConstraint(SpringLayout.NORTH, lblSeed, 3, SpringLayout.NORTH, textFieldSeed);
					springLayout.putConstraint(SpringLayout.WEST, lblSeed, 0, SpringLayout.WEST, lblConfidencefactor);
					springLayout.putConstraint(SpringLayout.EAST, lblSeed, -209, SpringLayout.EAST, this);
					paramPanel.add(lblSeed);
					
					chckbxUnpruned = new JCheckBox("unpruned");
					springLayout.putConstraint(SpringLayout.NORTH, chckbxUnpruned, 6, SpringLayout.SOUTH, textFieldSeed);
					springLayout.putConstraint(SpringLayout.WEST, chckbxUnpruned, 0, SpringLayout.WEST, chckbxBinarysplits);
					springLayout.putConstraint(SpringLayout.EAST, chckbxUnpruned, -293, SpringLayout.EAST, this);
					paramPanel.add(chckbxUnpruned);
					
					JButton btnRun= new JButton("Run");
					springLayout.putConstraint(SpringLayout.NORTH, btnRun, 32, SpringLayout.SOUTH, chckbxUnpruned);
					springLayout.putConstraint(SpringLayout.WEST, btnRun, 0, SpringLayout.WEST, chckbxBinarysplits);
					btnRun.addActionListener(this); // Add listener.
					paramPanel.add(btnRun);
					
					
					parameterDialog.setSize(200, 400);
					parameterDialog.setLocation(this.getLocation().x + this.getWidth(), this.getLocation().y);
					parameterDialog.setVisible(true);
					parameterDialog.setResizable(false);
					parameterDialog.setContentPane(paramPanel);
				}
				else
				{
					parameterDialog.setVisible(true);
				}
			}
			else if(e.getActionCommand().equalsIgnoreCase("run")) 
			{
				// Window (parameterDialog) has been created in the if-case above, so hide it.
				this.parameterDialog.setVisible(false);
				// Hide parameter dialog if it exists.
				if(this.parameterErrorDialog != null)
				{
					this.parameterErrorDialog.setVisible(false);
				}
				
				// Fetch parameter values from parameter components.
				// These components have been created, so no need to check.
				ParametersPART parameters = new ParametersPART();
				parameters.setBinarySplitsFlag(this.chckbxBinarysplits.isSelected());
				parameters.setReducedErrorPruningFlag(this.chckbxReducederrorpruning.isSelected());
				parameters.setUnprunedFlag(this.chckbxUnpruned.isSelected());
				
				try
				{
					parameters.setConfidenceFactor(Float.parseFloat(this.textFieldConfidenceFactor.getText()));
					parameters.setMinNumObj(Integer.parseInt(this.textFieldMinNumObj.getText()));
					parameters.setNumFolds(Integer.parseInt(this.textFieldNumFolds.getText()));
					parameters.setSeed(Integer.parseInt(this.textFieldSeed.getText()));
				}
				catch(NumberFormatException ex)
				{
					if(this.parameterErrorDialog == null)
					{
						this.createParameterErrorDialog();
					}
					else
					{
						// Change title.
						this.parameterErrorDialog.setTitle("Error!");
						this.parameterErrorDialog.setVisible(true);
					}
					String errorText = "Could not convert contents of textfield to a number.\n";
					errorText += ex.getMessage();
					this.parameterErrorTextArea.setText(errorText);
					this.parameterDialog.setVisible(true);
					
					return;
				}
				
				// Pass parameters to machine learner.
				// (Data has always been loaded at this point).
				String results;
				try
				{
					MachineLearner.DecisionList decisionList = learner.generateRules(parameters);

					// TODO this should probably be done once, but for now it's useful
					// to be able to change the mapping without restarting application
					// or reloading dataset etc.
					SolutionMap solutions = new SolutionMap("src/data/solutionmap.txt");

					results = solutions.matches(decisionList);
				} 
				catch (Exception exception)
				{
					results = "Failed to generate rules: " + exception.getMessage();
					exception.printStackTrace();
				}
				
				// Create new, window for showing the results.
				if(resultsDialog == null)
				{
					resultsDialog = new JDialog(this);
					resultsDialog.setSize(this.getSize());
					resultsDialog.setLocation(this.getLocation().x + this.getWidth(), this.getLocation().y);
					resultsDialog.setVisible(true);
					resultsDialog.setResizable(true);
					
					// Create a panel for it.
					JPanel resultsPanel = new JPanel();
					resultsPanel.setLayout(new BorderLayout(0, 0));
					
					// Create a text area to show the results in.
					resultsTextArea = new JTextArea();
					resultsTextArea.setEditable(false);
					
					// Put it in a scroll pane.
					JScrollPane scrollPane = new JScrollPane(resultsTextArea);
					scrollPane.getVerticalScrollBar().setUnitIncrement(vSBSpeed);
					scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
					scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
					
					// Add scroll pane to panel.
					resultsPanel.add(scrollPane);
					
					// Add/set panel to window.
					resultsDialog.setContentPane(resultsPanel);
				}
				else
				{
					resultsDialog.setVisible(true);
				}
				
				// Show the results (output) in window.
				resultsTextArea.setText(results);
				resultsTextArea.setCaretPosition(0); // Scroll to the top.
			}
		}
		else if(e.getActionCommand().equalsIgnoreCase("export..."))
		{
			// First check if a dataset has been loaded.
			if(!this.learner.hasDataset())
			{
				this.textAreaOtherTab.setText("No data to save; no dataset has been (successfully) loaded!\n");
				this.tabbedPane.setSelectedIndex(1);
				return;
			}

			// Set filter.
			FileFilter filter = new FileNameExtensionFilter("Text (.txt)", "txt");
			this.fileChooser.resetChoosableFileFilters();
			this.fileChooser.setFileFilter(filter);
			
			// Choose file name and location.
			int returnVal = this.fileChooser.showSaveDialog(this);
			if(returnVal == JFileChooser.APPROVE_OPTION)
			{
				File file = fileChooser.getSelectedFile();
				boolean saveFile = true;
				// Let user decide to overwrite existing file.
				if(file.exists()) 
				{
					int response = JOptionPane.showConfirmDialog(this, "Are you sure you want to override existing file?", "Confirm", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
					if(response == JOptionPane.NO_OPTION || response == JOptionPane.CLOSED_OPTION) 
					{
						saveFile = false;
					}
				}
				// Save session information.
				if(saveFile)
				{
					String textExport = "";
					String fileName = file.getAbsolutePath().replaceFirst("[.][^.]+$", ""); // Remove extension (if any).
					fileName += "." + ((FileNameExtensionFilter)fileChooser.getFileFilter()).getExtensions()[0]; // Add extension from filter.
					// Create stream/open file.
					FileOutputStream outputStream = null;
					try 
					{
						outputStream = new FileOutputStream(fileName);
					} 
					catch (FileNotFoundException e1) 
					{
						textExport += "FAILED to save file: '" + fileName + ".";
						textExport += e1.getMessage();
					}
					// Output data.
					if(outputStream != null)
					{
						String outputText = "";
						
						outputText += "Dataset used:\n";
						outputText += this.dataSetFileName + "\n";
						outputText += "-----------------------\n";
						outputText += "Dataset filter used:\n";
						outputText += this.datasetFilter.toString() + "\n";
						outputText += "-----------------------\n";
						outputText += "Machine learning parameters used:\n";
						if(parameters != null)
						{
							outputText += this.parameters.toString() + "\n";
						}
						else
						{
							outputText += "None (machine learner was never used). \n";
						}
						// TODO causes & solutions.
						//outputText += "s:\n";
						//outputText += this.learner.getInstancesAsString() + "\n";
						//outputText += this.learner.getInstanceInformation() + "\n";
						//outputText += causes;
						//outputText += solutions;
						
						try 
						{
							outputStream.write(outputText.getBytes());
						} 
						catch (IOException e1) 
						{
							textExport += "FAILED to save file: '" + fileName + ".";
							textExport += e1.getMessage();
						}
						
						textExport += "File: '" + fileName + "' saved!";
						try 
						{
							outputStream.close();
						} 
						catch (IOException e1) 
						{
							textExport += "FAILED to close file!";
							textExport += e1.getMessage();
						}
						
						// Use the dialog for showing parameter errors to show export info.
						if(this.parameterErrorDialog == null)
						{
							this.createParameterErrorDialog();
						}
						else
						{
							this.parameterErrorDialog.setVisible(true);
						}
						// Change title.
						this.parameterErrorDialog.setTitle("Export.");
						this.parameterErrorTextArea.setText(textExport);
					}
				}
			}
		}
		else if(e.getActionCommand().equalsIgnoreCase("quit"))
		{
			this.dispose(); 
		}
		else if(e.getActionCommand().contains("Apply"))
		{
			// First check if a data set has been loaded.
			if(!this.learner.hasDataset())
			{
				this.textAreaOtherTab.setText("No data to apply filter on; no dataset has been (successfully) loaded!\n");
				this.tabbedPane.setSelectedIndex(1);
				return;
			}
			
			if(e.getActionCommand().equalsIgnoreCase("apply default"))
			{
				// For a list of reasons to why these are irrelevant, see the project report.
				String[] irrelevantAttributes =
				{
					"OlycksID",
					"Ar",
					"Manad",
					"Lan",
					"Kommun",
					"Vagtyp_vag_B",
					"Trafikelement1",
					"Trafikelement2",
					"Svarhetsgrad"
					//"Olyckstyp"
					// todo: year.
				};
				
				// Set flag of these attributes.
				for(int i = 0; i < irrelevantAttributes.length; i++)
				{
					try 
					{
						this.attributeTree.setAttributeToUse(irrelevantAttributes[i], false);
						// ev. Todo: Update GUI tree.
						
					} 
					catch (Exception e1) 
					{
						System.out.println(e1.getMessage());
					}
				}
			}
			else
			{
				// Get which attribute values that shall be used for the filter from the GUI tree.
				this.loadAttributeTreeFromGUITree(this.attributeTree);
			}
			
			// Create data set filter and apply it.
			DatasetFilter datasetFilter = new DatasetFilter(this.attributeTree);
			this.learner.filterAndScan(datasetFilter);
			
			// Output filtered data.
			textOtherTab += this.learner.getInstanceInformation();	
			
			// Table test
			this.instancesTableModel.setDataVector(this.learner.getInstances(), this.learner.getAttributes());
		}
		else if(e.getActionCommand().contains("Set"))
		{
			if(e.getActionCommand().equalsIgnoreCase("set class attribute..."))
			{
				// First check if a dataset (attributes) has been loaded.
				if(!this.learner.hasDataset())
				{
					this.textAreaOtherTab.setText("No attributes to select; no dataset has been (successfully) loaded!\n");
					this.tabbedPane.setSelectedIndex(1);
					return;
				}
				
				// Create new/show window for setting class attribute.
				if(this.setClassAttributeDialog == null)
				{
					this.setClassAttributeDialog = new JDialog(this, "Choose class attribute.");
					JPanel classPanel = new JPanel();
					classPanel.setLayout(null);
	
					// Get attribute names.
					// TODO?: getClassAttributes();
					String[] attributeNames = this.attributeTree.getAttributes();
					if(attributeNames.length < 1)
					{
						this.textAreaOtherTab.setText("No attributes to select; No attributes loaded.\n");
						this.tabbedPane.setSelectedIndex(1);
						return;
					}
					// Create and add buttons to button group and panel.
					JRadioButton[] radioButtons = new JRadioButton[attributeNames.length];
					int y = 6;
					// TODO?: getClassAttributes();
					for(int i = attributeNames.length - 2; i < attributeNames.length; i++)
					{
						radioButtons[i] = new JRadioButton(attributeNames[i]);
						radioButtons[i].setBounds(6, y, 400, 23);
						this.radioButtonGroup.add(radioButtons[i]);
						classPanel.add(radioButtons[i]);
						y += 26;
					}
					// Add normal button for sending command to use the selected radio button.
					JButton setButton = new JButton("Set");
					setButton.addActionListener(this);
					setButton.setBounds(6, y, 60, 23);
					classPanel.add(setButton);
					y += 26 * 3;
					
					// Set dialog settings.
					this.setClassAttributeDialog.setSize(200, y);
					this.setClassAttributeDialog.setLocation(this.getLocation().x + this.getWidth() / 2 - 100, this.getLocation().y + this.getHeight() / 2 - y / 2);
					this.setClassAttributeDialog.setVisible(true);
					this.setClassAttributeDialog.setResizable(true);
					this.setClassAttributeDialog.setContentPane(classPanel);
				}
				else
				{
					this.setClassAttributeDialog.setVisible(true);
				}
			}
			else if(e.getActionCommand().equalsIgnoreCase("Set"))
			{
				if(this.radioButtonGroup.getSelection() == null)
				{
					// Use the dialog for alerting the user that no attribute has been selected.
					if(this.parameterErrorDialog == null)
					{
						this.createParameterErrorDialog();
					}
					else
					{
						this.parameterErrorDialog.setVisible(true);
					}
					// Change title.
					this.parameterErrorDialog.setTitle("Set class attribute.");
					this.parameterErrorTextArea.setText("No attribute has been chosen as class attribute; choose an attribute!");
				}
				else
				{
					// Find attribute name of selected radio button (attribute).
					Enumeration<AbstractButton> en = this.radioButtonGroup.getElements();
					AbstractButton nextElement = en.nextElement();
					String attributeName = nextElement.getText();
					while(en.hasMoreElements() && !nextElement.isSelected())
					{
						nextElement = en.nextElement();
						attributeName = nextElement.getText();
					}
					// Find index of attribute name.
					int index = this.attributeTree.getAttributeIndex(attributeName);
					// Set class index for the machine learning algorithm.
					try
					{
						this.learner.setClassIndex(index);
						
						// Use the dialog for telling the user everything went ok.
						if(this.parameterErrorDialog == null)
						{
							this.createParameterErrorDialog();
						}
						else
						{
							this.parameterErrorDialog.setVisible(true);
						}
						// Change title.
						this.parameterErrorDialog.setTitle("Success!");
						this.parameterErrorTextArea.setText("Class attribute was set to: " + attributeName + "(" + index + ")!\n");
						
						// Hide set class attribute dialog.
						this.setClassAttributeDialog.setVisible(false);
					}
					catch(IndexOutOfBoundsException ex)
					{
						// Use the dialog for alerting the user that an attribute could not be chosen as class attribute.
						if(this.parameterErrorDialog == null)
						{
							this.createParameterErrorDialog();
						}
						else
						{
							this.parameterErrorDialog.setVisible(true);
						}
						// Change title.
						this.parameterErrorDialog.setTitle("Set class attribute.");
						this.parameterErrorTextArea.setText("Failed to set class attribute; index is out of bounds!");
					}
				}
			}
		}
		else
		{
			textOtherTab += "UNKNOWN COMMAND\n";
			textOtherTab += e.getActionCommand();
		}
		
		// Show the data (input) in the text areas.
		if(!textOtherTab.equals(""))
		{
			this.textAreaOtherTab.setText(textOtherTab);
			this.textAreaOtherTab.setCaretPosition(0); // Scroll to the top.
		}		
		
		//Create piecharts
		this.updatePieCharts();
			
	}
	
	void updatePieCharts()
	{
		this.pieChartPanel.removeAll();
		
		int[][] attributeCounts = learner.getAttributeValueCount();
		String[][] attributeValues = learner.getAttibuteValues();
		int[] missingValues = learner.getMissingValues();
		
		for(int i = 0; i < this.learner.getNrOfAttributes(); i++)
		{
			//Create the pie chart
			GridBagConstraints gbc = new GridBagConstraints();	
			gbc.gridy = i / 3 * 2;
			gbc.gridx = i % 3 * 3 + 1;
			gbc.anchor = GridBagConstraints.NORTHWEST;
			gbc.gridheight = 2;
			
			//Color[] colors = new Color[attributeCounts[i].length + 1];
			Vector<Color> colors = new Vector<Color>();
			float[] percentages = new float[attributeCounts[i].length + 1];
			Random r = new Random(1);
			
			//Generate colors
			for(int j = 0; j < attributeCounts[i].length; j++)
			{
				Color c = new Color(r.nextInt(255), r.nextInt(255), r.nextInt(255));
				percentages[j] = 100.0f * (float)attributeCounts[i][j] / (float)learner.getNrOfInstances();
				
				if(j > 0)
				{
					//Loop until a unique color that isn't too similar to its neighbors has been found, or a maximum number of iterations has been done
					int maxIter = 200;
					
					while((PieChart.colorIsUnique(c, colors) == false || Math.abs((c.getRed() + c.getGreen() + c.getBlue()) - (colors.lastElement().getRed() + colors.lastElement().getGreen() + colors.lastElement().getBlue())) < 30) && maxIter > 0)
					{
						c = new Color(r.nextInt(255), r.nextInt(255), r.nextInt(255));
						maxIter--;
					}
				}
				
				colors.add(c);

				//Desaturated color test
				//int desatColor = (c.getRed() + c.getGreen() + c.getBlue()) / 3;
				//colors.add(new Color(desatColor, desatColor, desatColor));
			}
			
			colors.add(Color.LIGHT_GRAY);
			percentages[attributeCounts[i].length] = 100.0f * (float)missingValues[i] / (float)learner.getNrOfInstances();
			
			this.pieChartPanel.add(new PieChart(300, 300, percentages, colors.toArray(new Color[0])), gbc);		

			//Add the name of the attribute
			gbc = new GridBagConstraints();
			gbc.anchor = GridBagConstraints.CENTER;
			gbc.gridy = i / 3 * 2;
			gbc.gridx = i % 3 * 3;				
			
			JLabel attributeTitle = new JLabel(learner.getAttributes()[i]);
			attributeTitle.setFont(attributeTitle.getFont().deriveFont(Font.BOLD));
			
			this.pieChartPanel.add(attributeTitle, gbc);
			
			//Add some attribute statistics
			gbc = new GridBagConstraints();
			gbc.anchor = GridBagConstraints.NORTHWEST;
			gbc.gridy = i / 3 * 2 + 1;
			gbc.gridx = i % 3 * 3;		
			
			JPanel p = new JPanel();
			p.setLayout(new GridLayout(20, 2, 1, 1));
			p.setOpaque(true);
			p.setBackground(Color.WHITE);
			
			this.pieChartPanel.add(p, gbc);
			
			//Create labels for the attribute values
			for(int j = 0; j < attributeCounts[i].length; j++)
			{
				JLabel label = new JLabel(attributeValues[i][j] + "(" + attributeCounts[i][j] + ")");
				label.setForeground(colors.get(j));

				p.add(label);
			}	
			
			JLabel label = new JLabel("Missing(" + missingValues[i] + ")");
			label.setForeground(Color.LIGHT_GRAY);

			p.add(label);			
		}	
		
		this.otherPanel.updateUI();
	}
}
