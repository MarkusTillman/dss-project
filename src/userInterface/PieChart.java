package userInterface;

import javax.swing.*;
import java.awt.*;
import java.util.Vector;

public class PieChart extends JComponent
{
    private int m_width;
    private int m_height;
    private int[] m_arcs;
    private Color[] m_colors;
    
    public PieChart(int width, int height)
    {
        this.m_width = width;
        this.m_height = height;  
        
        this.m_arcs = new int[0];
        this.m_colors = new Color[0];        
        
        this.setPreferredSize(new Dimension(0, 0));
    } 
    
    public PieChart(int width, int height, float values[], Color[] colors)
    {
        this.m_width = width;
        this.m_height = height;
        
        int length = Math.min(values.length, colors.length);
        
        this.m_arcs = new int[length];
        this.m_colors = new Color[length];
        float valueSum = 0;
        int percentRemaining = 360;
        
        //Calculate the sum of the values
        for(int i = 0; i < length; i++)
        {
        	valueSum += values[i];
        }       
        
        //Initial percentage calculation
        for(int i = 0; i < length; i++)
        {
            this.m_colors[i] = colors[i];
            this.m_arcs[i] = (int)(360.0f * values[i] / valueSum);
            percentRemaining -= this.m_arcs[i];
        } 
        
        //Add in the remaining percent
        if(percentRemaining < 360)
        {        
	        int i = 0;
	        
	        while(percentRemaining > 0)
	        {
	        	if(this.m_arcs[i % length] > 0)
	        	{
	        		this.m_arcs[i % length]++;
	        		percentRemaining--;
	        	}	        	
	        	
	        	i++;
	        }
        }
               
        this.setPreferredSize(new Dimension(width, height));
    }
    
    static boolean colorIsUnique(Color c, Vector<Color> otherColors)
    {
    	boolean result = true;
    	
    	for(int i = 0; i < otherColors.size() && result == true; i++)
    	{
			if(c.equals(otherColors.get(i)) == true)
			{
				result = false;
			}
    	}
    	
    	return result;
    }
    
    void setData(float values[], Color[] colors)
    {
        int length = Math.min(values.length, colors.length);
        
        this.m_arcs = new int[length];
        this.m_colors = new Color[length];
        float valueSum = 0;
        int percentRemaining = 360;
        
        //Calculate the sum of the values
        for(int i = 0; i < length; i++)
        {
        	valueSum += values[i];
        }       
        
        //Initial percentage calculation
        for(int i = 0; i < length; i++)
        {
            this.m_colors[i] = colors[i];
            this.m_arcs[i] = (int)(360.0f * values[i] / valueSum);
            percentRemaining -= this.m_arcs[i];
        } 
        
        //Add in the remaining percent
        for(int i = 0; i < percentRemaining; i++)
        {
        	this.m_arcs[i % length]++;
        }     
        
        this.setPreferredSize(new Dimension(this.m_width, this.m_height));        
    }
    
    @Override
    protected void paintComponent(Graphics g)
    {
        //Clear the component
        g.clearRect(0, 0, this.getWidth(), this.getHeight());
        
        int angle = 0;
        
        for(int i = 0; i < this.m_arcs.length; i++)
        {    
            g.setColor(this.m_colors[i]);
            g.fillArc(0, 0, this.m_width, this.m_height, angle, this.m_arcs[i]);
            
            angle += this.m_arcs[i];
        }
    }
}
